<?php 
/**
 * Shortcode for social icons 
 * user [tplshare]
 */

defined( 'ABSPATH' ) || exit; // Exit if direct file access

add_shortcode( 'tplshare',  function() {
    return $this->get_template();
});